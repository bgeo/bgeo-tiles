const MapKey = "pk.eyJ1IjoiYnJicm93bmdlbyIsImEiOiJjaWoyMDN2Nm8wMGNudTRtNDl6c2ZiaGRtIn0.KVnzC-9Zk5memMTjEkeWkA";
mapboxgl.accessToken = MapKey;
// sanfrancisco
//var ll = new mapboxgl.LngLat(-122.477266, 37.685649);
//switzerland
var ll = new mapboxgl.LngLat(8.5500000, 47.3666700);
var map = new mapboxgl.Map({
    container: 'map',
    center: ll,
    zoom: 14,
    style: 'minimal.json'
});
map.addControl(new mapboxgl.Navigation({position: 'top-left'}));