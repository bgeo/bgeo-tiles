ZURICH = https://osm2vectortiles-downloads.os.zhdk.cloud.switch.ch/v1.0/extracts/zurich.mbtiles
SAN_FRANCISCO = https://osm2vectortiles-downloads.os.zhdk.cloud.switch.ch/v1.0/extracts/san_francisco.mbtiles

san_francisco: vector_tiles/san_francisco.mbtiles
zurich: vector_tiles/zurich.mbtiles

vector_tiles/san_francisco.mbtiles:
	mkdir -p $(dir $@)
	wget "$(SAN_FRANCISCO)" -O $@

vector_tiles/zurich.mbtiles:
	mkdir -p $(dir $@)
	wget "$(ZURICH)" -O $@