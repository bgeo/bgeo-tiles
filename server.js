var express = require('express'),
    http = require('http'),
    path = require('path'),
    async = require('async'),
    tilelive = require('tilelive'),
    mbtiles = require('mbtiles'),
    app = express();
require('mbtiles').registerProtocols(tilelive);

var TILES_PATH = path.join("mbtiles:", __dirname, "vector_tiles");
var TILES = [
    path.join(TILES_PATH, "zurich.mbtiles"),
    path.join(TILES_PATH, "san_francisco.mbtiles")
];
var TILE_MAP = {};

async.map(TILES, tilelive.load, function(err, results){
    if (err) console.log(err);
    var i = 0;
    for (var tile of TILES) {
        var fileName = tile.replace(/^.*[\\\/]/, '').split(".")[0];
        TILE_MAP[fileName] = results[i];
        i++
    }
    app.set('port', 3000);

    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });


    app.get('/:name/:z/:x/:y.vector.pbf$', function(req, res){
        var z = req.params.z;
        var x = req.params.x;
        var y = req.params.y;
        var name = req.params.name;

        console.log('get tile %s, %d, %d, %d', name, z, x, y);

        TILE_MAP[name].getTile(z, x, y, function(err, tile, headers) {
            if (err) {
                res.status(404);
                res.send(err.message);
                console.log(err.message);
            } else {
                res.set(headers);
                res.send(tile);
            }
        });
    });

    http.createServer(app).listen(app.get('port'), "127.0.0.1");
});